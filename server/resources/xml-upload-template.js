"use strict";
exports.__esModule = true;
exports.DITTemplate = void 0;
var DITTemplate = /** @class */ (function () {
    function DITTemplate() {
    }
    DITTemplate.getTemplate = function (sourceInfo, exportInfo, profile) {
        return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n    <hmis:Sources xmlns:airs=\"http://www.clarityhumanservices.com/schema/2020/1/AIRS_3_0_mod.xsd\"\n                  xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n                  xmlns:hmis=\"http://www.clarityhumanservices.com/schema/2020/1/HUD_HMIS.xsd\">\n    <hmis:Source>\n      <hmis:SourceID>" + sourceInfo.SourceID + "</hmis:SourceID>\n      <hmis:SourceType>" + sourceInfo.SourceType + "</hmis:SourceType>\n      <hmis:SourceName>" + sourceInfo.SourceName + "</hmis:SourceName>\n      <hmis:SoftwareName>" + sourceInfo.SoftwareName + "</hmis:SoftwareName>\n      <hmis:SoftwareVersion>" + sourceInfo.SoftwareVersion + "</hmis:SoftwareVersion>\n      <hmis:SourceContactEmail>" + sourceInfo.SourceContactEmail + "</hmis:SourceContactEmail>\n      <hmis:SourceContactFirst>" + sourceInfo.SourceContactFirstName + "</hmis:SourceContactFirst>\n      <hmis:SourceContactLast>" + sourceInfo.SourceContactLastName + "</hmis:SourceContactLast>\n      <hmis:Export>\n      <hmis:ExportID>" + exportInfo.ExportID + "</hmis:ExportID>\n      <hmis:ExportDate>" + exportInfo.ExportDate + "</hmis:ExportDate>\n      <hmis:ExportPeriod>\n        <hmis:StartDate>" + exportInfo.ExportStartDate + "</hmis:StartDate>\n        <hmis:EndDate>" + exportInfo.ExportEndDate + "</hmis:EndDate>\n      </hmis:ExportPeriod>\n      <hmis:ExportPeriodType>reportingPeriod</hmis:ExportPeriodType>\n      <hmis:ExportDirective>deltaRefresh</hmis:ExportDirective>\n\n      <hmis:Client hmis:dateCreated=\"" + new Date().toISOString().slice(0, 19) + "\"\n                    hmis:dateUpdated=\"" + new Date().toISOString().slice(0, 19) + "\"\n                    hmis:userID=\"" + sourceInfo.UserID + "\">\n        " + profile + "\n      </hmis:Client>\n      </hmis:Export>\n    </hmis:Source>\n    </hmis:Sources>";
    };
    return DITTemplate;
}());
exports.DITTemplate = DITTemplate;
