
// Node server.
import express from 'express';
import cors from 'cors';
import axios, { Method } from 'axios';
import fs from 'fs';

// Local libraries.
import { AuthenticateLooker, Credentials } from './server-services/auth';
import { DIT } from './server-services/clarity-xml';

// Load the project configuration file.
const projectConfig = JSON.parse(fs.readFileSync('./project_settings/config.json').toString());

// Get DIT and Looker credentials.
const credentials = Credentials.get(projectConfig.credentialsPath);

// Login to Looker.
const lookerAuth = new AuthenticateLooker(projectConfig.lookerBaseUrl, credentials.looker.lookerUserName, credentials.looker.lookerSecret);

// Start the Node server.
const app = express();

// Setup Clarity DIT.
const dit = new DIT(projectConfig.instanceName);

// Add middleware for handling requests.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));

// Serve the Angular application.
app.use(express.static(process.cwd() + '/dist/dit-api-clarity-connect-demo/'));
app.get('/', (req, res) => {
  res.sendFile(projectConfig.projectFolder + 'index.html');
});

// Create DIT upload endpoint.
app.post('/clients', (req, res) => {

  const uploadXML = dit.createUpload(req.body);
  dit.uploadXML(credentials.dit.ditApiLogin, credentials.dit.ditApiKey, uploadXML).then((result) => {
    console.log(result);
    res.send(result);
  }).catch((err) => {
    res.send(err);
  });
});

// Proxy the Looker API for the UI.
app.all('/looker-api/*', (req, res) => {

  const lookerServiceUrl = req.url.replace('/looker-api/', projectConfig.lookerBaseUrl);
  const headers = {
    'Content-Type': 'application/json;charset=utf-8',
    Authorization: `token ${lookerAuth.getAccessToken()}`
  };

  axios({method: req.method.toLowerCase() as Method,
        url: lookerServiceUrl,
        headers,
        data: req.body
  }).then((result) => {
    res.send(result.data);
  }).catch((e) => {
    res.send(e);
  });
});

// Start server.
app.listen(projectConfig.port, () => {
    console.log(`Server listening on the port::${projectConfig.port}`);
});
