import os
from io import StringIO
import pandas as pd

import looker_sdk 

###############
# Authenticate
###############
# Clarity Instance Parameters

# API Credentials
# Pull credentials from a safely stored text file
# on your machine.
# Remove your API keys from the text file when finished with your tasks. 

sdk = looker_sdk.init40(config_file="looker.ini")

#############
# Send Query
#############
# Define your query

# view: base = HMIS Performance
# view: client = Coordinated Entry
# view: clients = Services
# view: client_model = Client Model
# view: data_quality = Data Quality Model
# view: agencies = Project Descriptor Model
# view: 

query_body = mdls.WriteQuery(
    model= "demo_connection_model",
    view= "client",                         
    fields= [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ],
    filters={"clients.added_date": "after 2020-01-01"},
    limit= -1
    )

################
# Get Response
################
# Run query
response = sdk.run_inline_query(
    result_format= "csv",
    body = query_body
    )


# Convert to CSV.
df = pd.read_csv(StringIO(response))

