# Clarity Two-Way Integration App Setup Instructions
The Clarity Two-Way Integration is based on two extremely popular frameworks:

* [Angular](https://angular.io/)
* [Node](https://nodejs.org/en/about/)

Angular handles all the fancy user interface buttons and forms, while Node handles serving the application and low-level API calls--such as Looker and Clarity DIT.  These two frameworks are popular because of their reliability and ease of use.

Below are thorough instructions intended to guide anyone, regardless of technical experience, through setting up a copy of the Two-Way Clarity app.  However, once setup, you will need to learn how to program TypeScript, use the Angular framework, and manage a Node server.

Ok! Let's get started.

# Prepare Environment
Before we can run the application code we must setup what's known as a "development environment," or just "environment."  A workbench is a good metaphor; it is a place where we can work on our application and all our tools are ready for use.

## Install NodeJS

Let's setup NodeJS.  

Visit the download link below:

* [NodeJS Download](https://nodejs.org/en/download/)

And select the installer appropriate for your operation system--this article is written assuming you are using Windows.

![download-nodejs-windows](images/install_nodejs_windows.png)

Once you've downloaded the installer, open it.

![download-nodejs-exec](images/install_nodejs_exec.png)

Follow the instructions, leaving everything as default, except, check the box asking, "Automatically install necessary tools."

![download-nodejs-wizard](images/nodejs_install_extra_tools.png)

## Install Visual Studio Code
There are many code editors available, so feel free to use any you wish.  However, we recommend Visual Studio Code.  It is provided by Microsoft and free to use.

To download it, visit:
* [Download VSCode](https://code.visualstudio.com/download)

And run the installer.

![vscode-exec](images/vscode_exec.png)

Once Visual Studio Code is installed, you should be able to open it from your Start Menu.

First part done, on to setting up the project!

# Project Setup
Now we have our general environment setup, we need to make a copy of the start project provided by Bitfocus.  This project is stored in version control and to access it we will need to install the version control tool Git.

## Installing Git
Git is an old and free program which allows easy management of software versions.  To install it, visit:

* [Git Download](https://git-scm.com/download/win)

And click the link `64-bit Git for Windows Setup`

![install-git](images/install_git.png)

Once the download is complete, double-click and install it.  After the installation is complete, go the Start Menu and type `cmd` to open the command prompt.  At the command prompt type
```
git --version
```
If you see something like
```
git version 2.24.3
```
Git has been successfully installed on your machine.

## Get the Starter Project
One of the more difficult tasks when creating an integrated application is getting the basics down, this is where Bitfocus is your best friend!  We have created a skeleton project with all the basics in place--all you have to do is make a copy of the project and away you go!

To get your copy, visit the project page at:

* [angular-dit-looker-demo-app](https://gitlab.com/bitfocus-public/clarity-two-way-integration-demo/-/tree/master)

And find the `Clone` button at the top right.  Then, copy the text under `Clone with HTTPS`, this is the address we will provide to the Git program to make your very own copy of the start project.

![clone-project](./images/clone_project.png)

With the link in your clipboard, open the command prompt again and type
```
git clone https://gitlab.com/bitfocus-public/data-analysis/demonstrations/angular-dit-looker-demo-app/-/tree/master
```
Git will fetch all of the application files and make a copy on your local machine, under the folder `angular-dit-looker-demo-app`.  Let's take a look!

## Open the Project
Go to your Start Menu and open Visual Studio Code.  Once open, let's open your new application folder.  Go to `File -> Open Folder...`

![open-project-folder](images/open_folder_vscode.png)

Find the folder containing your copy of the application code and click `Select Folder`.

![vscode-download](images/select_folder_vscode.png)

Visual Studio Code should open with all of the project files listed out to the left.

![vscode-project-files](images/vscode_project_files.png)

And now press `F1` to bring up the Visual Studio Code command menu.  Select `Create New Integrated Terminal (In Active Workspace)`.  This will create a convenient command prompt inside our project, allowing us to run the commands needed to finish setup, and eventually, to run the application. 

![create-terminal](images/create_integrated_terminal_vscode.png)

## Install Resource Packages
We still need to finish setting up the project.  In your integrated command prompt type
```
npm install
```
This will install all the additional packages our code depends on.

![npm-install](images/npm_install.gif)

We also need to install the tools which will serve our application locally, while we work on developing it.  We can do this by typing at the command prompt
```
npm install -g nodemon @angular/cli
```

![install-angular-cli-and-nodemon](images/angular_cli_and_nodemon.gif)

## Credentials
Almost there!

To enable our application to use the Looker and DIT APIs, we must provide our application with login credentials.  We can do this by creating a file in the `project_settings` folder called `creds.json`.  You can create a file in the folder by right-clicking on the folder and select `Create File`.

![add-credentials-json-file](images/add_creds_json.png)

After we have created this file, we can copy the contents of the file called `example_creds.json`, which also is in the `project_settings` folder.  

### Clarity DIT API Credentials
You can find the DIT API credentials inside your Clarity instance by visiting `Manage -> Sharing`.  There, towards the bottom, you will find the `API Login` and the `API Key`, which is what an application needs to communicate with the DIT API.  Paste these, respectively, into the new `creds.json` file.

![add-clarity-dit-credentials](images/dit_credentials.png)

## Looker API Credentials
Like the DIT, the Looker API has its own set of credentials.  Unfortunately, these are only available by request.  Your System Administrators can request Looker API tokens by contacting the Clarity Help Desk.

**Note, use of the Looker API is based on specific use cases.  And is subject to change.**

![add-looker-api-credentials](images/looker_api_credentials.png)

## Test Application
Whew! A lot of setup, time for the pay.  If everything has gone well, we should be able to type

```
nodemon
```

And press `Return`.  This should take 1-5 minutes to build the application.  After it is built, you should see

```
Server listening on the port::2048
```

Appear in the command prompt.  This means the application is ready!  You can open any browser and visit http://localhost:2048 and should see your newly running application.

![start-dit-looker-project](images/project_working.gif)

